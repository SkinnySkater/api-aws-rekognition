
from flask import Flask, request, jsonify
from flask_restful import Resource, Api, abort
import logging
from awsCall import getLeafInformations

app = Flask(__name__)
app.config.from_object('config')
api = Api(app)

file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)


class Leaf(Resource):
    def post(self):
        image = request.form.get('data_image')
        #name = request.form.get('file_name')
        json_res = getLeafInformations(app, image, "")
        return json_res, json_res['code']


api.add_resource(Leaf, '/{}/floralitiks/recognition/leaf'.format(app.config['API_VERSION']))


if __name__ == '__main__':
    app.run(host=app.config['FLASK_SERVER_HOST'], port=app.config['PORT_SERVER'])
