import boto3
from urllib.request import urlretrieve
from mimetypes import guess_extension


def treat_json(code, msg, data):
    req_resp = {
        'code': code,
        'message': msg,
        "data": data
    }
    return req_resp


def getLeafInformations(app, key_image, name):
    client = boto3.client(
        'rekognition',
        aws_access_key_id=app.config['AWS_ACCESS_KEY'],
        aws_secret_access_key=app.config['AWS_SECRET_ACCESS_KEY'],
        region_name="us-east-1", )
    filename = ""
    try:
        filename, data = urlretrieve(key_image)
        print(filename, guess_extension(data.get_content_type()))
    except Exception as e:
        print(e)
        return treat_json(403, "KO", str(e))

    try:
        imgfile = open(filename, 'rb')
        imgbytes = imgfile.read()
        imgfile.close()
    except Exception as e:
        print(e)
        print('There was an error opening the image')
        return treat_json(401, "KO", str(e))

    imgobj = {'Bytes': imgbytes}

    data_response = client.detect_faces(Image=imgobj, Attributes=['ALL'])
    print(data_response)
    return treat_json(201, "OK", data_response)
