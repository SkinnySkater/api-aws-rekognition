# README #

This is a quick sample to bind flask api to made microservice like architecture to AWS services.

### What is this repository for? ###

* very Simple API Route made in flask-restfull, which call the AWS Rekognition API the result will be send in JOSN.
* Version: 0.1
* Python Version: 3.6
* [AWS Rekognition](https://us-east-2.console.aws.amazon.com/rekognition/home)
* [AWS Challenge](https://innovationforamission.agorize.com/fr/challenges/innovation-for-a-mission/teams?q=&hPP=33&idx=aws_student_challenge_teams&p=1)


### How do I get set up? ###

* Install: git clone project, install python :3 
* Install python AWS CLI [AWS Rekognition](https://github.com/boto/boto3#quick-start) 
* Configuration: go to your dashboard and generate your cli Identification credentials [Here](https://console.aws.amazon.com/machinelearning/home?region=us-east-1#/) and copy past the credentials in the config.py
* Dependencies: pip install -r requirement
